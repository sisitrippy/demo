#!/bin/bash

normal=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)

# normalizes according to docker hub organization/image naming conventions:
function normalize(){
  printf "$1" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9._-]//g'
}

## get latest tag in specified branch
function latest_tag(){
  local TAG=$(git describe --tags --abbrev=0 2>/dev/null)
  printf "$TAG"
}

## get latest revision number
function latest_revision(){
  local REV=$(git rev-list --count HEAD 2>/dev/null)
  echo "$REV"
}

## extract current branch name
function current_branch(){
  local BRANCH=$(git rev-parse --abbrev-ref HEAD | cut -d"/" -f2)
  printf "$BRANCH"
}

function highest_tag(){
  local TAG=$(git tag --list 2>/dev/null | sort -V | tail -n1 2>/dev/null)
  printf "$TAG"
}

## extract tag commit hash code, tag name provided by argument
function tag_hash(){
  local TAG_HASH=$(git log -1 --format=format:"%H" $1 2>/dev/null | tail -n1)
  printf "$TAG_HASH"
}

## get latest/head commit hash number
function head_hash(){
  local COMMIT_HASH=$(git rev-parse --verify HEAD)
  printf "$COMMIT_HASH"
}

## increment PATCH part, reset all other lower PARTS, don't touch STAGE
function increment_patch(){
  PARTS[2]=$(( PARTS[2] + 1 ))
  PARTS[3]=0
  IS_DIRTY=1
}

## increment MINOR part, reset all other lower PARTS, don't touch STAGE
function increment_minor(){
  PARTS[1]=$(( PARTS[1] + 1 ))
  PARTS[2]=0
  PARTS[3]=0
  IS_DIRTY=1
}

## increment MAJOR part, reset all other lower PARTS, don't touch STAGE
function increment_major(){
  PARTS[0]=$(( PARTS[0] + 1 ))
  PARTS[1]=0
  PARTS[2]=0
  PARTS[3]=0
  IS_DIRTY=1
}


## parse last found tag, extract it PARTS
function parse_last(){
  local position=$(($1-1))

  # two parts found only
  local SUBS=( ${PARTS[$position]//-/ } )
  #echo ${SUBS[@]}, size: ${#SUBS}

  # found NUMBER
  PARTS[$position]=${SUBS[0]}
  #echo ${PARTS[@]}

  # found SUFFIX
  if [[ ${#SUBS} -ge 1 ]]; then
    PARTS[4]=${SUBS[1],,} #lowercase
    #echo ${PARTS[@]}, ${SUBS[@]}
  fi
}

# compose version from PARTS
function compose(){
  MAJOR="${PARTS[0]}"
  MINOR=".${PARTS[1]}"
  PATCH=".${PARTS[2]}"
  REVISION=".${PARTS[3]}"
  SUFFIX="-${PARTS[4]}"

  # if empty {PATCH}
  if [[ "${#PATCH}" == 1 ]]; then
    PATCH=""
  fi

  echo "${MAJOR}${MINOR}${PATCH}" #full format
}

# initial version used for repository without tags
INIT_VERSION=0.0.0

# do GIT data extracting
TAG=$(latest_tag)
REVISION=$(latest_revision)
BRANCH=$(current_branch)
TOP_TAG=$(highest_tag)
TAG_HASH=$(tag_hash $TAG)
HEAD_HASH=$(head_hash)

# do we have any GIT tag for parsing?!
printf ""
if [[ -z "${TAG// }" ]]; then
  TAG=$INIT_VERSION
  printf "${red}No tags found.${normal}"
else
  printf "\nFound tag: %s in branch '%s'\n" $TAG $BRANCH
fi

# print current revision number based on number of commits
printf "Current Revision: %s\n" $REVISION
printf "Current Branch: %s\n\n" $BRANCH

PARTS=( ${TAG//./ } )
parse_last ${#PARTS[@]} # array size as argument
#echo ${PARTS[@]}

# if no parameters than emulate --default parameter
if [[ "$@" == "" ]]; then
  set -- $NO_ARGS_VALUE
fi

# # parse input parameters
# for i in "$@"
# do
#   key="$i"

#   case $key in
#     -a|--alpha)                 # switched to ALPHA
#     PARTS[4]="alpha"
#     IS_SHIFT=1
#     ;;
#     -b|--beta)                  # switched to BETA
#     PARTS[4]="beta"
#     IS_SHIFT=1
#     ;;
#     -c|--release-candidate)     # switched to RC
#     PARTS[4]="rc"
#     IS_SHIFT=1
#     ;;
#     -r|--release)               # switched to RELEASE
#     PARTS[4]=""
#     IS_SHIFT=1
#     ;;
#     -p|--patch)                 # increment of PATCH
#     increment_patch
#     ;;
#     -e|--revision)              # increment of REVISION
#     increment_revision
#     ;;
#     -g|--git-revision)          # use git revision number as a revision part§
#     PARTS[3]=$(( REVISION ))
#     IS_DIRTY=1
#     ;;
#     -i|--minor)                 # increment of MINOR by default
#     increment_minor
#     ;;
#     --default)                 # stay on the same stage, but increment only last found PART of version code
#     increment_last_found
#     ;;
#     -m|--major)                 # increment of MAJOR
#     increment_major
#     ;;
#     -s|--stay)                  # extract version info
#     IS_DIRTY=1
#     NO_APPLY_MSG=1
#     ;;
#     --apply)
#     DO_APPLY=1
#     ;;
#     -h|--help)
#     help
#     ;;
#   esac
#   shift
# done

increment_patch
# increment_minor
# increment_major
DO_APPLY=1

# detected shift, but no increment
if [[ "$IS_SHIFT" == "1" ]]; then
    # temporary disable stage shift
    stage=${PARTS[4]}
    PARTS[4]=''

    # detect first run on repository, INIT_VERSION was used
    if [[ "$(compose)" == "0.0" ]]; then
        increment_minor
    fi

    PARTS[4]=$stage
fi

# no increment applied yet and no shift of state, do minor increase
if [[ "$IS_DIRTY$IS_SHIFT" == "" ]]; then
    increment_minor
fi

# instruct user how to apply new TAG
# echo -e "Proposed TAG: \033[32m$(compose)\033[0m"
printf "Proposed TAG: ${green}%s${normal}\n\n" $(compose)
# echo ''

if [[ "$NO_APPLY_MSG" == "" ]]; then
    echo 'following command(s) will executed in background automatically:'
    echo -e "\033[90m"
    echo "  ===> git tag $(compose)"
    echo "  ===> git push origin $(compose)"
    echo -e "\033[0m"
fi

# should we apply the changes
if [[ "$DO_APPLY" == "1" ]]; then
    echo "Done!"
    # echo "Applying git repository version up... no push, only local tag assignment!"
    echo ''

    # git tag $(compose)

    # confirm that tag applied
    # git --no-pager log --pretty=format:"%h%x09%Cblue%cr%Cgreen%x09%an%Creset%x09%s%Cred%d%Creset" -n 2 --date=short | nl -w2 -s"  "
    # echo ''
    # echo ''
fi