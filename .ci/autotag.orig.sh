#!/bin/bash

# normalizes according to docker hub organization/image naming conventions:
function normalize(){
  printf "$1" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9._-]//g'
}

# get latest tag in specified branch
function latest_tag(){
  local tag=$(git describe --tags --abbrev=0 | sed 's/[^0-9._-]//g' 2>/dev/null)
  printf "$tag"
}

# get latest revision number
function latest_revision(){
  local rev=$(git rev-list --count HEAD 2>/dev/null)
  printf "$rev"
}

# extract current branch name
function current_branch(){
  local branch=$(git rev-parse --abbrev-ref HEAD | cut -d"/" -f2)
  printf "$branch"
}

function highest_tag(){
  local tag=$(git tag --list 2>/dev/null | sort -V | tail -n1 2>/dev/null)
  printf "$tag"
}

# extract tag commit hash code, tag name provided by argument
function tag_hash(){
  local tag_hash=$(git log -1 --format=format:"%H" $1 2>/dev/null | tail -n1)
  printf "$tag_hash"
}

# get latest/head commit hash number
function head_hash(){
  local commit_hash=$(git rev-parse --verify HEAD)
  printf "$commit_hash"
}

# increment patch part, reset all other lower PARTS, don't touch StagE
function increment_patch(){
  PARTS[2]=$(( PARTS[2] + 1 ))
  PARTS[3]=0
  IS_DIRTY=1
}

# increment minor part, reset all other lower PARTS, don't touch StagE
function increment_minor(){
  PARTS[1]=$(( PARTS[1] + 1 ))
  PARTS[2]=0
  PARTS[3]=0
  IS_DIRTY=1
}

# increment major part, reset all other lower PARTS, don't touch StagE
function increment_major(){
  PARTS[0]=$(( PARTS[0] + 1 ))
  PARTS[1]=0
  PARTS[2]=0
  PARTS[3]=0
  IS_DIRTY=1
}

# parse last found tag, extract it PARTS
function parse_last(){
  local position=$(($1-1))

  # two parts found only
  local SUBS=( ${PARTS[$position]//-/ } )

  # found NUMBER
  PARTS[$position]=${SUBS[0]}
}

# compose version from PARTS
function compose(){
  major="${PARTS[0]}"
  minor=".${PARTS[1]}"
  patch=".${PARTS[2]}"

  # if empty {patch}
  if [[ "${#patch}" == 1 ]]; then
    patch=""
  fi

  printf "${major}${minor}${patch}" #full format
}

function init(){
  # initial version used for repository without tags
  init_version=0.0.0

  # do git data extracting
  tag=$(latest_tag)
  revision=$(latest_revision)
  branch=$(current_branch)
  top_tag=$(highest_tag)
  tag_hash=$(tag_hash $tag)
  head_hash=$(head_hash)

  # do we have any git tag for parsing?!
  printf ""
  if [[ -z "${tag// }" ]]; then
    printf "No tags found. %s\n"
    exit 1
  else
    printf "\nFound tag: \x1b[33m%s\x1b[0m in branch \x1b[33m'%s'\x1b[0m\n" "v$tag" "$branch"
  fi
  status=$?

  if [[ $status -eq 0 ]]; then
    # print current revision number based on number of commits
    printf "Current Branch: %s\n" $branch
    printf "Current Changes: %s\n" $revision

    PARTS=( ${tag//./ } )
    parse_last ${#PARTS[@]} # array size as argument

    # local version_identifier=$(git log -1 --pretty=format:%B | grep -Po 'release\([A-Za-z]*\)' | sed 's/.*(\(.*\))/\1/')
    local version_identifier=$(echo 'release(major)' | grep -Po 'release\([A-Za-z]*\)' | sed 's/.*(\(.*\))/\1/')
    version_identifier=$(normalize $version_identifier)

    for version_up in $version_identifier
    do
      case "$version_up" in
        major|minor|patch)
            increment_$version_up "$@"
            # # detected shift, but no increment
            # if [[ "$IS_SHIFT" == "1" ]]; then
            #   # temporary disable stage shift
            #   stage=${PARTS[4]}
            #   PARTS[4]=''
            #   # detect first run on repository, init_version was used
            #   if [[ "$(compose)" == "0.0.0" ]]; then
            #       increment_minor
            #   fi
            #   PARTS[4]=$stage
            # fi
  
            # no increment applied yet and no shift of state, do minor increase
            # if [[ "$IS_DIRTY$IS_SHIFT" == "" ]]; then
            #     increment_minor
            # fi
  
            # instruct user how to apply new tag
            printf "\nUpdate: \x1b[32m%s\x1b[0m\n" "$version_identifier"
            printf "Proposed tag: \x1b[32m%s\x1b[0m\n\n" "v$(compose)"

            printf 'following command(s) will executed in background automatically:\n\n'
            printf "\x1b[90m  ===> git tag %s\x1b[0m\n" "v$(compose)"
            printf "\x1b[90m  ===> git push origin %s\x1b[0m\n\n" "v$(compose)"
            ;;
        *)  
            printf "\n\x1b[31mERROR:\x1b[0m\n"
            printf "\x1b[31mERROR:\x1b[0m Sorry, the version identifier type is not defined correctly.\n"
            printf "\x1b[31mERROR:\x1b[0m please fill commit message(s) according to the commit convention below to trigger this action.\n"
            printf "\x1b[31mERROR:\x1b[0m\n"
            printf "\x1b[31mERROR:\x1b[0m Identifier type available:\n"
            printf "\x1b[31mERROR:\x1b[0m  [+] major\n"
            printf "\x1b[31mERROR:\x1b[0m  [+] minor\n"
            printf "\x1b[31mERROR:\x1b[0m  [+] patch\n"
            printf "\x1b[31mERROR:\x1b[0m\n"
            printf "\x1b[31mERROR:\x1b[0m Commit convention:\n"
            printf "\x1b[31mERROR:\x1b[0m  [+] release(<identifier>): <description>\n"
            printf "\x1b[31mERROR:\x1b[0m\n"
            printf "\x1b[31mERROR:\x1b[0m Reference: https://www.conventionalcommits.org/en/\n"
            printf "\x1b[31mERROR:\x1b[0m\n\n"
            exit 1
            ;;
      esac
    done
  fi
}

function run(){
  init "$@"
  if [[ $init -eq 0 ]]; then
    # git tag -am "Tagging for release v$(compose)" "v$(compose)"
    # # git push --tags
    sleep 1
    printf "\nTagging for release \x1b[32m%s\x1b[0m, done.\n\n" "v$(compose)"
  fi
}

run "${@:-.}"