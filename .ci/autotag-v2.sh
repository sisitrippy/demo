#!/bin/bash

# normalizes according to docker hub organization/image naming conventions:
function normalize(){
  printf "$1" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9._-]//g'
}

# get latest tag in specified branch
function latest_tag(){
  local tag=$(git describe --tags --abbrev=0 2>/dev/null)
  printf "$tag" |  sed 's/[^0-9._-]//g'
}

# get latest revision number
function latest_revision(){
  local rev=$(git rev-list --count HEAD 2>/dev/null)
  printf "$rev"
}

# extract current branch name
function current_branch(){
  local branch=$(git rev-parse --abbrev-ref HEAD | cut -d"/" -f2)
  printf "$branch"
}

function highest_tag(){
  local tag=$(git tag --list 2>/dev/null | sort -V | tail -n1 2>/dev/null)
  printf "$tag"
}

# extract tag commit hash code, tag name provided by argument
function tag_hash(){
  local tag_hash=$(git log -1 --format=format:"%H" $1 2>/dev/null | tail -n1)
  printf "$tag_hash"
}

# get latest/head commit hash number
function head_hash(){
  local commit_hash=$(git rev-parse --verify HEAD)
  printf "$commit_hash"
}

# increment patch part, reset all other lower PARTS, don't touch StagE
function increment_patch(){
  PARTS[2]=$(( PARTS[2] + 1 ))
  PARTS[3]=0
}

# increment minor part, reset all other lower PARTS, don't touch StagE
function increment_minor(){
  PARTS[1]=$(( PARTS[1] + 1 ))
  PARTS[2]=0
  PARTS[3]=0
}

# increment major part, reset all other lower PARTS, don't touch StagE
function increment_major(){
  PARTS[0]=$(( PARTS[0] + 1 ))
  PARTS[1]=0
  PARTS[2]=0
  PARTS[3]=0
}

# parse last found tag, extract it PARTS
function parse_last(){
  local position=$(($1-1))

  # two parts found only
  local SUBS=( ${PARTS[$position]//-/ } )

  # found NUMBER
  PARTS[$position]=${SUBS[0]}
}

# compose version from PARTS
function compose(){
  major="${PARTS[0]}"
  minor=".${PARTS[1]}"
  patch=".${PARTS[2]}"

  if [[ "${#PATCH}" == 1 ]]; then # if empty {PATCH}
    PATCH=""
  fi

  printf "${major}${minor}${patch}" #full format
}

function init(){
  # initial version used for repository without tags
  init_version=0.0.0

  # do GIT data extracting
  tag=$(latest_tag)
  revision=$(latest_revision)
  branch=$(current_branch)
  top_tag=$(highest_tag)
  tag_hash=$(tag_hash $tag)
  head_hash=$(head_hash)

  # do we have any GIT tag for parsing?!
  if [[ -z "${tag// }" ]]; then
    tag=$init_version
    printf "\n\x1b[33mNo tags found.\x1b[0m\n"
  else
    printf "\nFound tag: \x1b[33m%s\x1b[0m in branch \x1b[33m'%s'\x1b[0m\n" "v$tag" "$branch"
  fi

  # print current revision number based on number of commits
  printf "Current Branch: %s\n" $branch
  printf "Current Changes: %s\n" $revision

  PARTS=( ${tag//./ } )
  parse_last ${#PARTS[@]} # array size as argument

  # local version_identifier=$(git log -1 --pretty=format:%B | grep -Po 'release\([A-Za-z]*\)' | sed 's/.*(\(.*\))/\1/')
  local version_identifier=$(echo 'release(major)' | grep -Po 'release\([A-Za-z]*\)' | sed 's/.*(\(.*\))/\1/')
  version_identifier=$(normalize $version_identifier)
  for version_up in $version_identifier
  do
    case "$version_up" in
      major|minor|patch)
          do_apply=""

          if [[ "$(compose)" == "0.0.0" ]]; then
            increment_patch
          else
            increment_$version_up "$@"
          fi

          if [[ "$tag_hash" == "$head_hash" ]]; then
            printf "\n\x1b[34mINFO:\x1b[0m\n"
            printf "\x1b[34mINFO:\x1b[0m [SUGGESTION]\n"
            printf "\x1b[34mINFO:\x1b[0m Looks like you never had a version tag on your project before.\n"
            printf "\x1b[34mINFO:\x1b[0m please create tag manually with specific SemVer naming convention you want, e.g. %s-alpha\n" "v$(compose)"
            printf "\x1b[34mINFO:\x1b[0m\n"
            printf "\x1b[34mINFO:\x1b[0m\x1b[90m  ===> git tag %s-alpha\x1b[0m\n" "v$(compose)"
            printf "\x1b[34mINFO:\x1b[0m\x1b[90m  ===> git push origin %s-alpha\x1b[0m\n" "v$(compose)"
            printf "\x1b[34mINFO:\x1b[0m\n"
            printf "\x1b[34mINFO:\x1b[0m Reference: https://semver.org/\n"
            printf "\x1b[34mINFO:\x1b[0m\n\n"
            do_apply="1"
          else
            printf "\nUpdate: \x1b[32m%s\x1b[0m\n" "$version_identifier"
            printf "Proposed tag: \x1b[32m%s\x1b[0m\n\n" "v$(compose)"
            printf 'following command(s) will executed in background automatically:\n\n'
            printf "\x1b[90m  ===> git tag %s\x1b[0m\n" "v$(compose)"
            printf "\x1b[90m  ===> git push origin %s\x1b[0m\n\n" "v$(compose)"
          fi
          ;;
      *)  
          printf "\n\x1b[31mERROR:\x1b[0m\n"
          printf "\x1b[31mERROR:\x1b[0m Sorry, the version identifier type is not defined correctly.\n"
          printf "\x1b[31mERROR:\x1b[0m please fill commit message(s) according to the commit convention below to trigger this action.\n"
          printf "\x1b[31mERROR:\x1b[0m\n"
          printf "\x1b[31mERROR:\x1b[0m Identifier type available:\n"
          printf "\x1b[31mERROR:\x1b[0m  [+] major\n"
          printf "\x1b[31mERROR:\x1b[0m  [+] minor\n"
          printf "\x1b[31mERROR:\x1b[0m  [+] patch\n"
          printf "\x1b[31mERROR:\x1b[0m\n"
          printf "\x1b[31mERROR:\x1b[0m Commit convention:\n"
          printf "\x1b[31mERROR:\x1b[0m  [+] release(<identifier>): <description>\n"
          printf "\x1b[31mERROR:\x1b[0m\n"
          printf "\x1b[31mERROR:\x1b[0m Reference: https://www.conventionalcommits.org/en/\n"
          printf "\x1b[31mERROR:\x1b[0m\n\n"
          exit 1
          ;;
    esac
  done
}

function run(){
  init "$@"
  if [[ $init -eq 0 ]]; then
    if [[ $do_apply -gt 0 ]]; then
      exit 1
    else
      git tag -am "Tagging for release v$(compose)" "v$(compose)"
      git push --tags
      printf "\nTagging for release \x1b[32m%s\x1b[0m, done.\n\n" "v$(compose)"
    fi
  fi
}

run "${@:-.}"