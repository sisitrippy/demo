#!/bin/bash

# normalizes according to docker hub organization/image naming conventions:
normalize() {
  echo "$1" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9._-]//g'
}

tags_release() {
  if [[ $BITBUCKET_TAG != @(v*|release-*) ]]; then
    printf >&2 'Error...\n'
    exit 1
  else
    eval "semver=\$(echo $BITBUCKET_TAG | sed 's/[[:alpha:]|(|[:space:]]//g' | awk -F- '{print $1}')"
    if [[ $semver =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
      printf >&2 'Retag the latest Docker images with SemVer...\n'
      # normalize the branch name:
      semver="$(normalize "$semver")"
      # re-tag latest image to semver
      sed -i 's/0.0.0/'"$semver"'/g' build-env
      make retag
    else
      printf >&2 'Error... Please build tag manually\n'
      exit 1
    fi
  fi
}

tags_branch() {
  branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
  if [[ "$branch" != @(development|staging|main|master) ]]; then
    printf >&2 'Error...\n'
    exit 1
  else
    # tag master as "latest":
    if [[ "$branch" = @(main|master) ]]; then
      branch='latest'
    fi
    # normalize the branch name:
    branch="$(normalize "$branch")"
  fi

  printf >&2 'Building Docker images with branch tags...\n'
  sed -i 's/branch/'"$branch"'/g' build-env
  make build-and-push
}

build_images(){
  if [ -z ${BITBUCKET_TAG+x} ]; then
    tags_branch
  else
    tags_release
  fi
}

init() {
  cp templates/build-env.tmpl build-env
  
  repository=${BITBUCKET_REPO_FULL_NAME%:*}  # retain the part before the colon
  project_name=${repository##*/}  # retain the part after the last slash
  
  if [[ -z "$BITBUCKET_REPO_SLUG" ]]; then
    export BITBUCKET_REPO_SLUG=$project_name
  fi

  sed -i 's/project_name/'"$BITBUCKET_REPO_SLUG"'/g' build-env
  build_images "$@"
}

init "${@:-.}"