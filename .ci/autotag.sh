#!/bin/bash

# normalizes according to docker hub organization/image naming conventions:
normalize() {
  echo "$1" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9._-]//g'
}

make_tag() {
  # normalize the branch name:
  semver="$(normalize "$semver")"

  release_number="v$semver"
  git tag -am "Tagging for release $release_number" $release_number
  git push --tags
}

validate_tag() {
  eval "semver=\$(git log -1 --pretty=format:%B | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}?\.*[0-9]{1,3}' | cut -d '-' -f 2-)"
  if [[ $semver =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    make_tag
  else
    printf >&2 'Error... Please build tag manually\n'
    exit 1
  fi
}

init() {
  cp templates/build-env.tmpl build-env
  validate_tag "$@"
}

init "${@:-.}"