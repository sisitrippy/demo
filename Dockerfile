FROM alpine:3.14 as development

COPY . .

FROM alpine:3.14 as staging

COPY . .

FROM alpine:3.14 as latest

COPY . .