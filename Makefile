include .ci/build-env

# default    default              
# running:
# linux/amd64, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/386, linux/arm/v7, linux/arm/v6

ifeq ($(UNAME),Darwin)
    SHELL   := /opt/local/bin/bash
    OS_X    := true
else ifneq (,$(wildcard /etc/redhat-release))
    OS_RHEL := true
else
    OS_DEB  := true
    SHELL   := /bin/bash
endif

DEFAULT_TAGS := "$(BRANCH)"
IMAGE_NAME := "$(OWNER)/$(PROJECT_NAME)"
NEW_IMAGE_NAME := "$(IMAGE_NAME):$(SEMVER)"

test2:
	@echo ${IMAGE_NAME}:${DEFAULT_TAGS}

build-and-push:
	@docker buildx build --push --target ${BRANCH} \
           --tag ${IMAGE_NAME}:${DEFAULT_TAGS} \
           --platform linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 .

#                 --push \
#                 --target ${STAGE} \
#                 --platform linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 \
#                 --tag ${IMAGE_NAME}:${DEFAULT_TAGS}
# →  docker buildx build --push --target ${STAGE} \                                           [808afbd]
#             --tag sisitrippy/multiarch-example:latest \
#             --platform linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 .
#	@docker buildx build \
#                 --push \
#                 --target ${STAGE} \
#                 --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
#                 --tag ${IMAGE_NAME}:${DEFAULT_TAGS}
#
#  - docker buildx build --target ${STAGE} --platform linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 -t ${IMAGE_NAME}:${DEFAULT_TAGS} --push .

retag:
	@docker buildx build --push \
           --tag ${IMAGE_NAME}:latest --tag ${NEW_IMAGE_NAME} \
           --platform linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 .
#	@docker pull ${IMAGE_NAME}:latest
#	@docker tag ${IMAGE_NAME}:latest ${NEW_IMAGE_NAME}
#	@docker push ${NEW_IMAGE_NAME}
