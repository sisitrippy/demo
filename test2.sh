# # # #ip=1.22.4.5

# # # eval "ip=\$(echo release-1.0.0.0.0 | grep -Eo '(release|version)-[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}?\.*[0-9]{1,3}' | cut -d '-' -f 2-)"

# # # if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
# # #   echo "success"
# # # else
# # #   echo "fail"
# # # fi

# # # ip=1.22.4.5.6
# # # ip="$(echo release-1.0.0.0.0 | grep -Eo '(release|version)-[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}?\.*[0-9]{1,3}' | cut -d '-' -f 2-)"
# # # eval "ip=\$(echo release-1.0.0.0.0 | grep -Eo '(release|version)-[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}?\.*[0-9]{1,3}' | cut -d '-' -f 2-)"
# # # echo $ip
# # # if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
# # #   echo "success"
# # # else
# # #   echo "fail"
# # # fi

# # # semver="$(echo release-1.0.1 | grep -Eo '^.*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}?\.*[0-9]{1,3}' | cut -d '-' -f 2-)"
# # # if [[ $semver =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
# # #   echo $semver
# # # else
# # #   printf >&2 'Error... Please build tag manually\n'
# # #   exit 1
# # # fi

# # eval "semver=\$(echo 'chore(release): 1.0.2' | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}?\.*[0-9]{1,3}' | cut -d '-' -f 2-)"
# # if [[ $semver =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
# #   echo $semver
# # else
# #   printf >&2 'Error... Please build tag manually\n'
# #   exit 1
# # fi

# semver=$(echo $BITBUCKET_TAG | sed 's/[[:alpha:]|(|[:space:]]//g' | awk -F- '{print $1}')
# eval "semver=\$(echo '"${BITBUCKET_TAG}"' | sed 's/[[:alpha:]|(|[:space:]]//g' | awk -F- '{print $1}')"
# echo $semver
# echo $semver2
# echo $BITBUCKET_TAG2
# if [[ $semver =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
#   printf >&2 'Retag the latest Docker images with SemVer...\n'
#   # normalize the branch name:
#   semver="$(normalize "$semver")"
#   echo $semver
#   # re-tag latest image to semver
#   # sed -i 's/0.0.0/'"$semver"'/g' build-env
#   # make retag
# else
#   printf >&2 'Error... Please build tag manually\n'
#   exit 1
# fi

if [[ -z "$BITBUCKET_REPO_SLUG" ]]; then
  export BITBUCKET_REPO_SLUG=$project_name
else
  sed -i 's/project_name/'"$project_name"'/g' build-env
fi