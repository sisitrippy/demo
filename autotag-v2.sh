#!/bin/bash

# normalizes according to docker hub organization/image naming conventions:
function normalize(){
  printf "$1" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9._-]//g'
}

function get_commit(){
  local COMMIT=$(git log -1 --pretty=format:%B | sed 's/.*(\(.*\))/\1/')
  printf "$COMMIT"
}

# get latest tag in specified branch
function latest_tag(){
  local TAG=$(git describe --tags --abbrev=0 2>/dev/null)
  printf "$TAG"
}

# get latest revision number
function latest_revision(){
  local REV=$(git rev-list --count HEAD 2>/dev/null)
  echo "$REV"
}

# extract current branch name
function current_branch(){
  local BRANCH=$(git rev-parse --abbrev-ref HEAD | cut -d"/" -f2)
  printf "$BRANCH"
}

function highest_tag(){
  local TAG=$(git tag --list 2>/dev/null | sort -V | tail -n1 2>/dev/null)
  printf "$TAG"
}

# extract tag commit hash code, tag name provided by argument
function tag_hash(){
  local TAG_HASH=$(git log -1 --format=format:"%H" $1 2>/dev/null | tail -n1)
  printf "$TAG_HASH"
}

# get latest/head commit hash number
function head_hash(){
  local COMMIT_HASH=$(git rev-parse --verify HEAD)
  printf "$COMMIT_HASH"
}

# increment PATCH part, reset all other lower PARTS, don't touch STAGE
function increment_patch(){
  PARTS[2]=$(( PARTS[2] + 1 ))
  PARTS[3]=0
  IS_DIRTY=1
}

# increment MINOR part, reset all other lower PARTS, don't touch STAGE
function increment_minor(){
  PARTS[1]=$(( PARTS[1] + 1 ))
  PARTS[2]=0
  PARTS[3]=0
  IS_DIRTY=1
}

# increment MAJOR part, reset all other lower PARTS, don't touch STAGE
function increment_major(){
  PARTS[0]=$(( PARTS[0] + 1 ))
  PARTS[1]=0
  PARTS[2]=0
  PARTS[3]=0
  IS_DIRTY=1
}


# parse last found tag, extract it PARTS
function parse_last(){
  local position=$(($1-1))

  # two parts found only
  local SUBS=( ${PARTS[$position]//-/ } )
  #echo ${SUBS[@]}, size: ${#SUBS}

  # found NUMBER
  PARTS[$position]=${SUBS[0]}
  #echo ${PARTS[@]}

  # found SUFFIX
  if [[ ${#SUBS} -ge 1 ]]; then
    PARTS[4]=${SUBS[1],,} #lowercase
    #echo ${PARTS[@]}, ${SUBS[@]}
  fi
}

# compose version from PARTS
function compose(){
  MAJOR="${PARTS[0]}"
  MINOR=".${PARTS[1]}"
  PATCH=".${PARTS[2]}"
  REVISION=".${PARTS[3]}"
  SUFFIX="-${PARTS[4]}"

  # if empty {PATCH}
  if [[ "${#PATCH}" == 1 ]]; then
    PATCH=""
  fi

  echo "${MAJOR}${MINOR}${PATCH}" #full format
}

function init(){
  # initial version used for repository without tags
  INIT_VERSION=0.0.0

  # do GIT data extracting
  TAG=$(latest_tag)
  REVISION=$(latest_revision)
  BRANCH=$(current_branch)
  TOP_TAG=$(highest_tag)
  TAG_HASH=$(tag_hash $TAG)
  HEAD_HASH=$(head_hash)

  # do we have any GIT tag for parsing?!
  printf ""
  if [[ -z "${TAG// }" ]]; then
    printf "${red}No tags found.${normal}"
  else
    printf "\nFound tag: %s in branch '%s'\n" $TAG $BRANCH
  fi

  # print current revision number based on number of commits
  printf "Current Revision: %s\n" $REVISION
  printf "Current Branch: %s\n\n" $BRANCH

  PARTS=( ${TAG//./ } )
  parse_last ${#PARTS[@]} # array size as argument

  increment_patch
  # increment_minor
  # increment_major
  # DO_APPLY=1

  # detected shift, but no increment
  if [[ "$IS_SHIFT" == "1" ]]; then
      # temporary disable stage shift
      stage=${PARTS[4]}
      PARTS[4]=''
      # detect first run on repository, INIT_VERSION was used
      if [[ "$(compose)" == "0.0" ]]; then
          increment_minor
      fi
      PARTS[4]=$stage
  fi

  # no increment applied yet and no shift of state, do minor increase
  if [[ "$IS_DIRTY$IS_SHIFT" == "" ]]; then
      increment_minor
  fi

  # instruct user how to apply new TAG
  # echo -e "Proposed TAG: \033[32m$(compose)\033[0m"
  printf "Proposed TAG: ${green}%s${normal}\n\n" $(compose)

  if [[ "$NO_APPLY_MSG" == "" ]]; then
    echo 'following command(s) will executed in background automatically:'
    echo -e "\033[90m"
    echo "  ===> git tag $(compose)"
    echo "  ===> git push origin $(compose)"
    echo -e "\033[0m"
  fi
}

function run(){
  init "$@"
  # if ! init; then
  #   echo "Could not execute function2" >&2
  #   # exit 1
  # fi
}

run "${@:-.}"