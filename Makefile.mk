#include .ci/build-env

ifeq ($(UNAME),Darwin)
    SHELL   := /opt/local/bin/bash
    OS_X    := true
else ifneq (,$(wildcard /etc/redhat-release))
    OS_RHEL := true
else
    OS_DEB  := true
    SHELL   := /bin/bash
endif

DEFAULT_TAGS := "$(BRANCH)"
IMAGE_NAME := "$(OWNER)/$(PROJECT_NAME)"
NEW_IMAGE_NAME := "$(IMAGE_NAME):$(SEMVER)"

#build-and-push: docker-build docker-push

build-and-push:
	@docker buildx build \
                 --push \
                 --target ${STAGE} \
                 --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
                 --tag ${IMAGE_NAME}:${DEFAULT_TAGS}
                 
# docker-push: docker-build
# 	@docker push ${IMAGE_NAME}:${DEFAULT_TAGS}

retag:
	@docker pull ${IMAGE_NAME}:latest
	@docker tag ${IMAGE_NAME}:latest ${NEW_IMAGE_NAME}
	@docker push ${NEW_IMAGE_NAME}
